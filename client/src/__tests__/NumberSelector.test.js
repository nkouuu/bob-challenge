import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import NumberSelector from '../components/NumberSelector/NumberSelector';

describe('NumberSelector tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  test('Render input with 0 value', () => {
    render(<NumberSelector name="bags" />);

    const input = screen.getByLabelText('bags');

    expect(input).toBeInTheDocument();
    expect(input.value).toBe('0');
  });

  test('Render input with default value', () => {
    render(<NumberSelector name="bags" defaultValue={3} />);

    const input = screen.getByLabelText('bags');

    expect(input).toBeInTheDocument();
    expect(input.value).toBe('3');
  });

  test('Input onChange is called on type and value updates', () => {
    const mockFn = jest.fn();
    render(<NumberSelector onChange={mockFn} name="bags" />);
    const input = screen.getByLabelText('bags');
    // Delete default 0 and type 5
    const text = '{backspace}5';
    userEvent.type(input, text);

    expect(mockFn).toHaveBeenCalled();
    expect(input.value).toBe('5');
  });

  test('Cannot type value bigger than maxValue', () => {
    render(<NumberSelector name="bags" maxValue={10} />);
    const input = screen.getByLabelText('bags');
    const text = '{backspace}12';
    userEvent.type(input, text);

    expect(input.value).toBe('1');
  });

  test('Cannot type value smaller than minValue', () => {
    render(<NumberSelector name="bags" minValue={5} />);
    const input = screen.getByLabelText('bags');
    const text = '{backspace}2';
    userEvent.type(input, text);

    expect(input.value).toBe('');
  });

  test('Only admits numbers', () => {
    render(<NumberSelector name="bags" />);
    const input = screen.getByLabelText('bags');
    const text = '{backspace}test';
    userEvent.type(input, text);

    expect(input.value).toBe('');
  });

  test('Render 2 buttons', () => {
    render(<NumberSelector name="bags" />);
    const buttons = screen.getAllByRole('button');

    expect(buttons.length).toBe(2);
  });

  test('Value increments on + button click', () => {
    render(<NumberSelector name="bags" />);
    const input = screen.getByLabelText('bags');
    const addButton = screen.getByText('+');
    expect(input.value).toBe('0');

    userEvent.click(addButton);

    expect(input.value).toBe('1');
  });

  test('Value decrements on - button click', () => {
    render(<NumberSelector name="bags" defaultValue={3} />);
    const input = screen.getByLabelText('bags');
    const subButton = screen.getByText('-');
    expect(input.value).toBe('3');

    userEvent.click(subButton);

    expect(input.value).toBe('2');
  });

  test('Subtract button must be disabled when value equals minValue', () => {
    render(<NumberSelector name="bags" minValue={3} defaultValue={3} />);
    const input = screen.getByLabelText('bags');
    const subButton = screen.getByText('-');

    expect(input.value).toBe('3');
    expect(subButton.disabled).toBe(true);
    userEvent.click(subButton);
    expect(input.value).toBe('3');
  });

  test('Add button must be disabled when value equals maxValue', () => {
    render(<NumberSelector name="bags" maxValue={3} defaultValue={3} />);
    const input = screen.getByLabelText('bags');
    const subButton = screen.getByText('+');

    expect(input.value).toBe('3');
    expect(subButton.disabled).toBe(true);
    userEvent.click(subButton);
    expect(input.value).toBe('3');
  });

  test('Buttons click triggers onChange with correct values', () => {
    const mockFn = jest.fn();
    render(<NumberSelector name="bags" defaultValue={3} onChange={mockFn} />);
    const addButton = screen.getByText('+');
    const subButton = screen.getByText('-');

    userEvent.click(addButton);
    userEvent.click(subButton);

    expect(mockFn).toHaveBeenCalledTimes(2);
    expect(mockFn).toHaveBeenCalledWith(4);
    expect(mockFn).toHaveBeenCalledWith(3);
  });
});
