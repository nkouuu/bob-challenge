import usersApi from '../../api/users.api';

export const CREATE_USER = 'CREATE_USER';
export const GET_ALL_USERS = 'GET_ALL_USERS';

export const createUserAction = data => {
  return async dispatch => {
    const res = await usersApi.createUser(data);
    dispatch({ type: CREATE_USER, payload: res.data });
  };
};

export const getAllUsersAction = () => {
  return async dispatch => {
    const res = await usersApi.getAllUsers();
    dispatch({ type: GET_ALL_USERS, payload: res.data });
  };
};
