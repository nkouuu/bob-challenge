import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import NewUserForm from '../components/NewUserForm/NewUserForm';

describe('NewUserForm  tests', () => {
  beforeAll(() => {
    jest.clearAllMocks();
  });

  test('Render form with name and bags inputs', async () => {
    render(<NewUserForm onSubmit={() => {}} />);

    const form = screen.getByLabelText('User form');
    const nameInput = screen.getByLabelText('name');
    const bagsInput = screen.getByLabelText('bags');

    expect(form).toBeInTheDocument();
    expect(nameInput).toBeInTheDocument();
    expect(bagsInput).toBeInTheDocument();
  });

  test('Render submit button', async () => {
    render(<NewUserForm onSubmit={() => {}} />);

    const button = screen.getByText('Add user');

    expect(button).toBeInTheDocument();
    expect(button.type).toBe('submit');
  });

  test('Cannot submit with empty values', async () => {
    const mockFn = jest.fn();
    render(<NewUserForm onSubmit={mockFn} />);

    const button = screen.getByText('Add user');
    const nameInput = screen.getByLabelText('name');
    expect(nameInput.value).toBe('');

    fireEvent.click(button);
    await waitFor(() => expect(mockFn).toHaveBeenCalledTimes(0));
  });

  test('Submit triggers with correct input values', async () => {
    const mockFn = jest.fn();
    render(<NewUserForm onSubmit={mockFn} />);

    const button = screen.getByText('Add user');
    const nameInput = screen.getByLabelText('name');
    const bagsInput = screen.getByLabelText('bags');

    const name = 'John Smith';
    const bags = 3;
    userEvent.type(nameInput, name);
    userEvent.type(bagsInput, `{backspace}${bags}`);

    fireEvent.click(button);

    await waitFor(() => expect(mockFn).toHaveBeenCalled());
    expect(mockFn).toHaveBeenCalledWith({ name, bags });
  });

  test('Submit dont triggers with invalid input values', async () => {
    const mockFn = jest.fn();
    render(<NewUserForm onSubmit={mockFn} />);

    const button = screen.getByText('Add user');
    const nameInput = screen.getByLabelText('name');
    const bagsInput = screen.getByLabelText('bags');

    const name = 'john';
    const bags = 7;
    userEvent.type(nameInput, name);
    userEvent.type(bagsInput, `{backspace}${bags}`);

    fireEvent.click(button);

    await waitFor(() => screen.getByLabelText('name-error'));

    expect(mockFn).toHaveBeenCalledTimes(0);
  });

  test('Incorrect values renders  disabled button', async () => {
    const mockFn = jest.fn();
    render(<NewUserForm onSubmit={mockFn} />);

    const button = screen.getByText('Add user');
    const nameInput = screen.getByLabelText('name');
    const bagsInput = screen.getByLabelText('bags');

    const name = 'john';
    const bags = '';
    userEvent.type(nameInput, name);
    userEvent.type(bagsInput, `{backspace}${bags}`);

    await waitFor(() => screen.getByLabelText('name-error'));

    expect(button.disabled).toBe(true);
  });

  test('Incorrect values renders error texts', async () => {
    const mockFn = jest.fn();
    render(<NewUserForm onSubmit={mockFn} />);

    const nameInput = screen.getByLabelText('name');
    const bagsInput = screen.getByLabelText('bags');

    const name = 'john';
    const bags = '';
    userEvent.type(nameInput, name);
    userEvent.type(bagsInput, `{backspace}${bags}`);

    await waitFor(() => screen.getByLabelText('name-error'));

    expect(screen.getByLabelText('name-error')).toBeInTheDocument();
    expect(screen.getByLabelText('bags-error')).toBeInTheDocument();
  });
});
