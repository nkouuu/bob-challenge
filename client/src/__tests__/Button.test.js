import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Button from '../components/Button/Button';

describe('Button tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('Render Button with label and text', () => {
    render(<Button label="Add user" text="Add user" />);

    const button = screen.getByLabelText('Add user');

    expect(button).toHaveTextContent('Add user');
  });

  test('Render disabled Button', () => {
    render(<Button text="Click here" disabled />);

    const button = screen.getByText('Click here');

    expect(button.disabled).toBe(true);
  });

  test('Click triggers onClick prop', () => {
    const mockFn = jest.fn();
    render(<Button text="Click here" onClick={mockFn} />);

    const button = screen.getByText('Click here');

    fireEvent.click(button);

    expect(mockFn).toHaveBeenCalled();
  });
});
