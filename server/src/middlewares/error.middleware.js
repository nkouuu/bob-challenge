import { STATUS_CODES } from 'http';

const errorHanler = function (err, req, res, next) {
  const status = err.status || 500;
  const error = err.message || STATUS_CODES[status];

  res.status(status);
  res.json({ error });
};

export default errorHanler;
