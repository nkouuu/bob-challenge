import Joi from 'joi';
import { objectIdSchema } from '../../constants/schemas';

export const create = Joi.object({
  name: Joi.string().min(3).max(30).required(),
  bags: Joi.number().integer().min(0).max(5)
});

export const update = Joi.object({
  name: Joi.string().min(3).max(30),
  bags: Joi.number().integer().min(0).max(5)
});

export const client = Joi.object({
  id: objectIdSchema.required(),
  name: Joi.string().min(3).max(30).required(),
  bags: Joi.number().integer().min(0).max(5).required()
});
