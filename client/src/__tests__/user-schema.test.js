import { userSchema } from '../utils/schemas';

let user = { name: 'John Smith', bags: 2 };

describe('User schema tests', () => {
  beforeEach(() => {
    user = { name: 'John Smith', bags: 2 };
  });

  test('Admits valid names', async () => {
    const valid = await userSchema.isValid(user);

    expect(valid).toBe(true);

    user.name = 'John smith';
    const valid2 = await userSchema.isValid(user);

    expect(valid2).toBe(true);

    user.name = 'Mike Johnsson Smith';
    const valid3 = await userSchema.isValid(user);

    expect(valid3).toBe(true);
  });

  test('Name must start with uppercase', async () => {
    user.name = 'john Smith';
    const valid = await userSchema.isValid(user);

    expect(valid).toBe(false);

    user.name = '  john Smith';
    const valid2 = await userSchema.isValid(user);

    expect(valid2).toBe(false);
  });

  test('Name must have 2 words', async () => {
    user.name = 'john';
    const valid = await userSchema.isValid(user);

    expect(valid).toBe(false);
  });

  test('Name doesnt admit numbers', async () => {
    user.name = 'Jo3n 1';

    const valid = await userSchema.isValid(user);
    expect(valid).toBe(false);
  });

  test('Name doesnt admit simbols', async () => {
    user.name = 'Jo_n Sanchez!';

    const valid = await userSchema.isValid(user);
    expect(valid).toBe(false);
  });

  test('Name doesnt admit initial or final backspace', async () => {
    user.name = '   John Sanchez ';

    const valid = await userSchema.isValid(user);
    expect(valid).toBe(false);
  });

  test('Admits valid bags', async () => {
    const valid = await userSchema.isValid(user);

    expect(valid).toBe(true);

    user.bags = 1;
    const valid2 = await userSchema.isValid(user);

    expect(valid2).toBe(true);

    user.bags = 5;
    const valid3 = await userSchema.isValid(user);

    expect(valid3).toBe(true);

    user.bags = 0;
    const valid4 = await userSchema.isValid(user);

    expect(valid4).toBe(true);

    user.bags = '2';
    const valid5 = await userSchema.isValid(user);

    expect(valid5).toBe(true);
  });

  test('Bags doesnt admit out of range or invalid values', async () => {
    user.bags = 7;
    const valid2 = await userSchema.isValid(user);

    expect(valid2).toBe(false);

    user.bags = -3;
    const valid3 = await userSchema.isValid(user);

    expect(valid3).toBe(false);

    user.bags = 22;
    const valid4 = await userSchema.isValid(user);

    expect(valid4).toBe(false);

    user.bags = '2a';
    const valid5 = await userSchema.isValid(user);

    expect(valid5).toBe(false);
  });
});
