import { BAD_REQUEST, CREATED, NOT_FOUND } from '../constants/http';
import { objectIdSchema } from '../constants/schemas';
import User from '../models/User/user.model';
import * as userSchemas from '../models/User/user.schemas';

export const getAllUsers = async (req, res, next) => {
  const users = await User.find({}, { name: 1, bags: 1 });
  res.json(users);
};

export const createUser = async (req, res, next) => {
  try {
    await userSchemas.create
      .required()
      .validateAsync(req.body)
      .catch(err => {
        res.status(BAD_REQUEST);
        throw new Error(err.details[0].message);
      });
    const newUser = await new User(req.body).save();
    res.status(CREATED);
    res.json(newUser);
  } catch (err) {
    next(err);
  }
};

export const updateUser = async (req, res, next) => {
  try {
    await objectIdSchema.validateAsync(req.params.id).catch(err => {
      res.status(BAD_REQUEST);
      throw new Error(err.details[0].message);
    });
    await userSchemas.update
      .required()
      .validateAsync(req.body)
      .catch(err => {
        res.status(BAD_REQUEST);
        throw new Error(err.details[0].message);
      });
    const user = await User.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    });
    if (user) {
      res.json(user);
    } else {
      res.status(NOT_FOUND);
      next();
    }
  } catch (err) {
    next(err);
  }
};

export const deleteUser = async (req, res, next) => {
  try {
    await objectIdSchema.validateAsync(req.params.id).catch(err => {
      res.status(BAD_REQUEST);
      throw new Error(err.details[0].message);
    });
    const user = await User.findByIdAndDelete(req.params.id, req.body);
    if (user) {
      res.json(user);
    } else {
      res.status(NOT_FOUND);
      next();
    }
  } catch (err) {
    next(err);
  }
};
