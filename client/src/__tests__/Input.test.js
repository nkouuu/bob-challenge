import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Input from '../components/Input/Input';

describe('Input tests', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('Render input', () => {
    render(<Input name="name" />);

    const input = screen.getByLabelText('name');

    expect(input).toBeInTheDocument();
    expect(input.value).toBe('');
  });

  test('Render input with placeholder', () => {
    render(<Input name="name" placeholder="Type here" />);

    const input = screen.getByPlaceholderText('Type here');

    expect(input).toBeInTheDocument();
  });

  test('Render disabled input', () => {
    render(<Input name="name" disabled />);

    const input = screen.getByLabelText('name');

    expect(input.disabled).toBe(true);
  });

  test('Typing triggers onChange prop and updated input value', () => {
    const mockFn = jest.fn();
    render(<Input name="name" onChange={mockFn} />);

    const input = screen.getByLabelText('name');
    const text = 'John';
    expect(input.value).toBe('');

    userEvent.type(input, text);

    expect(input.value).toBe(text);

    expect(mockFn).toHaveBeenCalled();
  });
});
