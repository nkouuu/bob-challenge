const Joi = require('joi');
const { OBJECT_ID_REGEX } = require('./regex');

export const objectIdSchema = Joi.string().regex(OBJECT_ID_REGEX);
