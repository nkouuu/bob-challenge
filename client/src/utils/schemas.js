import * as Yup from 'yup';
import { MAX_BAGS } from '../constants/limits';
import { NAME_REGEX } from '../constants/regex';

export const userSchema = Yup.object().shape({
  name: Yup.string()
    .matches(
      NAME_REGEX,
      'Name must start with a capital letter and have at least 2 words'
    )
    .max(30)
    .required(),
  bags: Yup.number()
    .min(0, 'Too Short!')
    .max(MAX_BAGS, 'Too Long!')
    .required('Required')
});
