import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import App from '../App';
import { initializeStore } from '../redux/store';
import usersMock from '../mocks/users.json';

const store = initializeStore();
const url = path => `http://localhost:4000/${path}`;

const server = setupServer(
  rest.get(url(`users`), (req, res, ctx) => {
    return res(ctx.json(usersMock));
  })
);

describe('New  tests', () => {
  beforeAll(() => {
    jest.clearAllMocks();
    server.listen();
  });
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test('Render new user form', async () => {});

  test('Render new user title', async () => {});

  test('On submit shows loading status', async () => {});

  test('On submit error shows error message', async () => {});

  test('On submit success shows back button and success title', async () => {});

  test('On back button click navigates home', async () => {});
});
