require('dotenv/config');
var express = require('express');
var cookieParser = require('cookie-parser');
var httpLogger = require('morgan');
const cors = require('cors');
const loaders = require('./loaders');

const { PORT = 4000 } = process.env;
var app = express();

// Config middlewares
app.use(cors());
app.use(httpLogger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

loaders.routes(app);
loaders.middlewares(app);

const server = app.listen(PORT, async () => {
  await loaders.mongodb();
  logger.info(`Server running and listening in port: ${PORT}`);
});

module.exports = { app, server };
