import errorHanler from '../middlewares/error.middleware';

export const middlewares = app => {
  app.use(errorHanler);
};
