const request = require('supertest');
const { app, server } = require('../src/app');
const Joi = require('joi');
const userSchemas = require('../src/models/User/user.schemas');
const {
  CREATED,
  SUCCESS,
  BAD_REQUEST,
  NOT_FOUND
} = require('../src/constants/http');

describe('/users', function () {
  beforeAll(async () => {});

  afterAll(() => {
    server.close();
  });

  const userMock = { name: 'Jane Doe', bags: 2 };

  it('GET / -> responds with array of jsons with correct schema', function (done) {
    request(app)
      .get('/users')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(async res => {
        const { body } = res;
        try {
          await Joi.array().items(userSchemas.client).validateAsync(body);
          done();
        } catch (err) {
          done(err);
        }
      });
  });

  it('POST -> create user and return it', function (done) {
    request(app)
      .post('/users')
      .send(userMock)
      .expect('Content-Type', /json/)
      .expect(CREATED)
      .then(res => {
        if (res.body) {
          done();
        } else {
          done(new Error('User not returned'));
        }
      })
      .catch(done);
  });

  it('POST -> dont allow user creation with wrong schema', function (done) {
    request(app)
      .post('/users')
      .send({ username: 'John Wick', age: 40 })
      .expect(BAD_REQUEST, done);
  });

  it('PUT -> updated user', function (done) {
    request(app)
      .get('/users')
      .then(({ body: users }) => {
        if (users && users.length) {
          const BAGS_NUMBER = 5;
          const user = users.find(e => e.bags !== BAGS_NUMBER);
          request(app)
            .put(`/users/${user.id}`)
            .send({ bags: BAGS_NUMBER })
            .expect('Content-Type', /json/)
            .expect(SUCCESS)
            .then(res => {
              expect(res.body.bags).toBe(5);
              done();
            })
            .catch(done);
        } else {
          done('User not found');
        }
      });
  });

  it('PUT -> cannot updated unexisting user ', function (done) {
    request(app)
      .put(`/users/827468937618274637893627`)
      .send({ bags: 2 })
      .expect(NOT_FOUND, done);
  });

  it('PUT -> cannot updated user with wrong schema', function (done) {
    request(app)
      .get('/users')
      .then(({ body: users }) => {
        if (users && users.length) {
          const user = users[0];
          request(app)
            .put(`/users/${user.id}`)
            .send({ username: 'John' })
            .expect(BAD_REQUEST, done);
        } else {
          done('Not available user');
        }
      });
  });

  it('DELETE -> delete user', function (done) {
    request(app)
      .get('/users')
      .then(({ body: users }) => {
        if (users && users.length) {
          const user = users[0];
          request(app).delete(`/users/${user.id}`).expect(SUCCESS, done);
        } else {
          done('User not found');
        }
      });
  });

  it('DELETE -> cannot delete unexisting user', function (done) {
    request(app)
      .delete(`/users/827468937618274637893627`)
      .expect(NOT_FOUND, done);
  });
});
