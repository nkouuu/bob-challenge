import apiInstance from './api-instance';

const url = (path = '') => `/users${path ? '/' : ''}${path}`;

const getAllUsers = () => apiInstance.get(url());

const createUser = data => apiInstance.post(url(), data);

const usersApi = { getAllUsers, createUser };

export default usersApi;
