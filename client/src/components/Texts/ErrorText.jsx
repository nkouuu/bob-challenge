import React from 'react';
import { string } from 'prop-types';

function ErrorText({ children, className, label }) {
  return (
    <span className={`text-red-500 text-lg ${className}`} aria-label={label}>
      {children}
    </span>
  );
}

ErrorText.propTypes = {
  className: string,
  label: string
};

ErrorText.defaultProps = {
  className: '',
  label: ''
};

export default ErrorText;
