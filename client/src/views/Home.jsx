import React, { useCallback, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import Button from '../components/Button/Button';
import User from '../components/User/User';
import { getAllUsersAction } from '../redux/users/actions';
import MessageText from '../components/Texts/MessageText';

const UsersList = ({ users }) => {
  const history = useHistory();

  const handleNewUserClick = useCallback(() => {
    history.push('/new');
  }, [history]);

  return (
    <div className="flex flex-col items-center w-full" data-testid="users-list">
      <div className="flex flex-wrap w-full">
        {users.map(({ name, bags, id }) => (
          <div className={`w-full md:w-1/3 p-6`} key={id}>
            <User name={name} bags={bags} className="my-16" />
          </div>
        ))}
      </div>
      <Button
        onClick={handleNewUserClick}
        className="bg-gray-100 p-4 text-xl"
        title="Add new user"
        text="Add new user"
        label="Add new user"
      />
    </div>
  );
};

const Home = ({ getAllUsers, users }) => {
  const [loading, setloading] = useState(false);
  const [error, seterror] = useState(false);

  useEffect(() => {
    getAllUsers()
      .then(() => {
        setloading(false);
      })
      .catch(() => {
        setloading(false);
        seterror(true);
      });
  }, [getAllUsers]);

  const { data: usersList } = users;
  const canRender = !loading && !error;
  return (
    <>
      {loading && <MessageText text="Loading..." />}
      {error && <MessageText text="Ups! Something went wrong" />}

      {canRender &&
        (usersList && usersList.length ? (
          <UsersList users={usersList} />
        ) : (
          <MessageText>We couldn`t find any users</MessageText>
        ))}
    </>
  );
};

export default connect(state => ({ users: state.users }), {
  getAllUsers: getAllUsersAction
})(Home);
