import mongoose from 'mongoose';

const { DB_URI } = process.env;

export const mongodb = async () => {
  try {
    const { connection } = await mongoose.connect(DB_URI);
    logger.info(`Connected to Mongo! Database name: ${connection.name}`);
  } catch (error) {
    logger.error(
      `Error connecting to mongo database, Error description: ${error}`
    );
  }
};
