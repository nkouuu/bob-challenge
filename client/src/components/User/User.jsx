import React from 'react';
import PropTypes from 'prop-types';

const User = ({ name, bags, className }) => {
  return (
    <div
      className={`flex flex-col items-center shadow-lg	rounded-xl p-6 ${className}`}
      data-testid="user"
    >
      <p className="text-2xl font-bold mb-4">{name}</p>
      <p className="text-2xl mb-4">{bags} bags</p>
    </div>
  );
};

User.propTypes = {
  name: PropTypes.string.isRequired,
  bags: PropTypes.number,
  className: PropTypes.string
};

User.defaultProps = {
  bags: 0,
  className: undefined
};

export default User;
