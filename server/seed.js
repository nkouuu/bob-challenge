require('dotenv').config({
  path: process.env.NODE_ENV === 'test' ? './.env.test' : './.env'
});
const mongoose = require('mongoose');
const { default: User } = require('./src/models/User/user.model');
const { mongodb } = require('./src/loaders/mongodb.loader');
require('./src/loaders/logger.loader');

const data = [
  { name: 'Dominic MaY' },
  { name: 'Stephanie	Hudson', bags: 3 },
  { name: 'Nicola	Mackay', bags: 2 },
  { name: 'Sally	North', bags: 5 },
  { name: 'Connor	Allan', bags: 1 }
];

mongodb().then(async () => {
  await User.collection.drop();
  await User.create(data);
  logger.info(`Seed created`);
  process.exit();
});
