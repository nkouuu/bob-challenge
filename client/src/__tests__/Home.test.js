import { render, screen, waitFor, fireEvent } from '@testing-library/react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { Provider } from 'react-redux';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import App from '../App';
import { initializeStore } from '../redux/store';
import usersMock from '../mocks/users.json';

const store = initializeStore();
const url = path => `http://localhost:4000/${path}`;

const server = setupServer(
  rest.get(url(`users`), (req, res, ctx) => {
    return res(ctx.json(usersMock));
  })
);

describe('Home tests', () => {
  beforeAll(() => {
    jest.clearAllMocks();
    server.listen();
  });
  afterEach(() => server.resetHandlers());
  afterAll(() => server.close());

  test('Render Home with loading message', async () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );

    const loading = screen.getByText('Loading...');

    expect(loading).toBeInTheDocument();

    await waitFor(() => screen.getByTestId('users-list'));
  });

  test('Render Home with error message', async () => {
    server.restoreHandlers();
    server.use(
      rest.get(url('users'), (req, res, ctx) => {
        return res(ctx.status(500));
      })
    );
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );

    await waitFor(() => screen.getByText('Ups! Something went wrong'));

    expect(screen.getByText('Ups! Something went wrong')).toBeInTheDocument();
  });

  test('Render Home with users', async () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );

    await waitFor(() => screen.getByTestId('users-list'));

    expect(screen.getAllByTestId('user').length).toBe(usersMock.length);
  });

  test('Render New User button', async () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );

    await waitFor(() => screen.getByLabelText('Add new user'));

    const button = screen.getByLabelText('Add new user');

    expect(button).toBeInTheDocument();
  });

  test('Navigate to /new on New User button click', async () => {
    const history = createMemoryHistory();
    render(
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );

    await waitFor(() => screen.getByLabelText('Add new user'));

    const button = screen.getByLabelText('Add new user');
    fireEvent.click(button);
    expect(history.location.pathname).toBe('/new');
  });
});
