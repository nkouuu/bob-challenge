import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router';
import Button from '../components/Button/Button';
import NewUserForm from '../components/NewUserForm/NewUserForm';
import { createUserAction } from '../redux/users/actions';

const NewUser = ({ createUser }) => {
  const history = useHistory();
  const [created, setcreated] = useState(false);
  const [loading, setloading] = useState(false);
  const [error, seterror] = useState(false);
  const handleFormSubmit = useCallback(
    async data => {
      setloading(true);
      createUser(data)
        .then(() => {
          setcreated(true);
          setloading(false);
        })
        .catch(() => {
          setloading(false);
          seterror(true);
        });
    },
    [createUser]
  );

  const handleButtonClick = useCallback(() => {
    history.push('/');
  }, [history]);

  return (
    <div className="flex flex-col items-center w-full p-8 py-12">
      <h2 className="text-4xl pb-4">
        {created ? 'User created!' : 'Add new user'}
      </h2>
      {created ? (
        <Button text="Back to list" onClick={handleButtonClick} />
      ) : (
        <NewUserForm
          onSubmit={handleFormSubmit}
          loading={loading}
          errorMsg={
            error ? 'Something went wrong, plese try again later.' : null
          }
        />
      )}
    </div>
  );
};

export default connect(() => ({}), { createUser: createUserAction })(NewUser);
