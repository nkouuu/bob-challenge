import React, { useCallback } from 'react';
import { func, bool, string, number } from 'prop-types';
import ErrorText from '../Texts/ErrorText';

function Input({
  disabled,
  onChange,
  className,
  tabIndex,
  placeholder,
  name,
  error
}) {
  const handleChange = useCallback(
    e => {
      const val = e.target.value;
      if (onChange) {
        onChange(val);
      }
    },
    [onChange]
  );
  return (
    <>
      <input
        onChange={handleChange}
        disabled={disabled}
        tabIndex={tabIndex}
        className={`p-4 rounded-lg border border-gray-300 ${
          disabled ? 'bg-gray-100' : ''
        } ${className}`}
        placeholder={placeholder}
        name={name}
        aria-label={name}
      />
      {error && <ErrorText label={`${name}-error`}>{error}</ErrorText>}
    </>
  );
}

Input.propTypes = {
  onChange: func,
  disabled: bool,
  className: string,
  tabIndex: number,
  placeholder: string,
  name: string,
  error: string
};

Input.defaultProps = {
  onChange: undefined,
  disabled: false,
  className: '',
  tabIndex: undefined,
  placeholder: 'Type...',
  name: undefined,
  error: undefined
};

export default Input;
