import { string } from 'prop-types';
import React from 'react';

const MessageText = ({ text }) => (
  <div className="w-full flex-1 flex items-center justify-center ">
    <span className="text-4xl text-bold">{text}</span>
  </div>
);

MessageText.propTypes = {
  text: string
};

export default MessageText;
