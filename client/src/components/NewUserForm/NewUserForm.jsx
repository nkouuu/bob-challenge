import React, { useCallback } from 'react';
import { bool, func, string } from 'prop-types';
import { Formik } from 'formik';
import NumberSelector from '../NumberSelector/NumberSelector';
import Button from '../Button/Button';
import Input from '../Input/Input';
import { MAX_BAGS } from '../../constants/limits';
import ErrorText from '../Texts/ErrorText';
import { userSchema } from '../../utils/schemas';

function NewUserForm({ onSubmit, loading, errorMsg }) {
  const handleSubmit = useCallback(
    ({ name, bags }) => {
      onSubmit({ name, bags });
    },
    [onSubmit]
  );
  return (
    <Formik
      onSubmit={handleSubmit}
      validationSchema={userSchema}
      initialValues={{}}
    >
      {({ values, errors = {}, handleSubmit, setFieldValue }) => {
        return (
          <form
            className="flex flex-col text-2xl py-12"
            onSubmit={handleSubmit}
            aria-label="User form"
          >
            <div className="flex flex-col my-4">
              <label className="pb-2">Name</label>
              <Input
                onChange={text => setFieldValue('name', text.trim())}
                name="name"
                error={errors.name}
                disabled={loading}
              />
            </div>
            <div className="flex flex-col">
              <label className="pb-2">Bags</label>
              <NumberSelector
                onChange={val => setFieldValue('bags', val)}
                name="bags"
                error={errors.bags}
                maxValue={MAX_BAGS}
                minValue={0}
                disabled={loading}
              />
            </div>
            <Button
              text="Add user"
              label="Add user"
              type="submit"
              disabled={loading || Object.keys(errors).length > 0}
              className="mt-6"
            />
            {errorMsg && <ErrorText>{errorMsg}</ErrorText>}
          </form>
        );
      }}
    </Formik>
  );
}

NewUserForm.propTypes = {
  onSubmit: func.isRequired,
  loading: bool,
  errorMsg: string
};
NewUserForm.defaultProps = {
  loading: false,
  errorMsg: null
};

export default NewUserForm;
