import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
      minLength: 3,
      maxLength: 30
    },
    bags: {
      type: Number,
      default: 0,
      min: 0,
      max: 10,
      validate: {
        validator: Number.isInteger,
        message: '{VALUE} must be an integer'
      }
    }
  },
  {
    timestamps: true,
    toJSON: {
      transform: (doc, ret) => {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      }
    }
  }
);

const User = mongoose.model('User', userSchema);

export default User;
