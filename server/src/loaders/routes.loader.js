const usersRouter = require('../routes/users.routes');

export const routes = app => {
  app.use('/users', usersRouter);
};
