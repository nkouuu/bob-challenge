import React from 'react';
import { func, string, oneOf, bool } from 'prop-types';

function Button({ text, onClick, className, type, disabled, title, label }) {
  return (
    <button
      onClick={onClick}
      className={`p-4 bg-blue-${
        disabled ? '200' : '500'
      } text-white rounded-lg text-xl ${className}`}
      type={type}
      disabled={disabled}
      title={title}
      aria-label={label}
    >
      {text}
    </button>
  );
}

Button.propTypes = {
  onClick: func,
  text: string.isRequired,
  className: string,
  type: oneOf(['button', 'submit']),
  disabled: bool,
  title: string,
  label: string
};

Button.defaultProps = {
  onClick: undefined,
  className: '',
  type: 'button',
  disabled: false,
  title: '',
  label: ''
};

export default Button;
