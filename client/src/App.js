import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import Home from './views/Home';
import NewUser from './views/New';

function App() {
  return (
    <div className="flex flex-col  min-h-screen">
      <header className="fixed w-full text-center text-6xl bg-blue-400 py-8 flex-shrink-0">
        <Link className="text-white" to="/">
          BoB coding challenge
        </Link>
      </header>
      <div className="flex flex-grow pt-40 pb-8">
        <Switch>
          <Route exact path={`/`} component={Home} />
          <Route exact path={`/new`} component={NewUser} />
        </Switch>
      </div>
      <footer className="h-24 bg-gray-500 flex-shrink-0 mt-auto" />
    </div>
  );
}

export default App;
