import React, { useCallback, useState } from 'react';
import { number, string, func, bool } from 'prop-types';
import './NumberSelector.css';
import ErrorText from '../Texts/ErrorText';

const roundedButtonBorderColor = 'gray-500';

const RoundedButton = ({ children, className, onClick, disabled, label }) => (
  <button
    className={`border border-${roundedButtonBorderColor} bg-gray-50 w-12 h-12 rounded-full text-2xl text-center font-extralight	text-${roundedButtonBorderColor} ${
      disabled ? 'opacity-25' : ''
    } ${className}`}
    onClick={onClick}
    disabled={disabled}
    type="button"
    aria-label={label}
  >
    {children}
  </button>
);

RoundedButton.propTypes = {
  children: string.isRequired,
  className: string,
  onClick: func,
  disabled: bool,
  label: string
};

RoundedButton.defaultProps = {
  className: '',
  onClick: undefined,
  disabled: false,
  label: ''
};

const NumberSelector = ({
  defaultValue,
  name,
  onChange,
  error,
  minValue,
  maxValue,
  label,
  disabled
}) => {
  const [value, setvalue] = useState(defaultValue);

  const matchMaxLimit = useCallback(
    val => maxValue === undefined || val <= maxValue,
    [maxValue]
  );

  const matchMinLimit = useCallback(
    val => minValue === undefined || val >= minValue,
    [minValue]
  );

  const handleChange = useCallback(
    e => {
      const val = e.target.value;
      const numberRegex = /^[0-9]+$/;
      if (
        val === '' ||
        (val.match(numberRegex) && matchMaxLimit(val) && matchMinLimit(val))
      ) {
        setvalue(val);
        if (onChange) {
          onChange(val ? Number(val) : val);
        }
      } else {
      }
    },
    [onChange, matchMaxLimit, matchMinLimit]
  );

  const add = useCallback(() => {
    setvalue(prevValue => {
      const val = Number(prevValue) + 1;
      if (onChange) {
        onChange(val);
      }
      return val;
    });
  }, [onChange]);

  const subtract = useCallback(() => {
    setvalue(prevValue => {
      const val = Number(prevValue) - 1;
      if (onChange) {
        onChange(val);
      }
      return val;
    });
  }, [onChange]);

  return (
    <>
      <span
        className={`flex justify-center items-center ${
          disabled ? 'opacity-25' : ''
        }`}
      >
        <RoundedButton
          onClick={subtract}
          disabled={minValue !== undefined && value === minValue}
          label="Subtract"
        >
          -
        </RoundedButton>
        <input
          type="number"
          value={value}
          title="Bags selector"
          className="text-xl text-center p-2 w-10 outline-none"
          onChange={handleChange}
          name={name}
          aria-label={label || name}
        />
        <RoundedButton
          onClick={add}
          disabled={maxValue !== undefined && value === maxValue}
          label="Add"
        >
          +
        </RoundedButton>
      </span>
      {error && (
        <ErrorText className="w-full" label={`${name}-error`}>
          {error}
        </ErrorText>
      )}
    </>
  );
};

NumberSelector.propTypes = {
  defaultValue: number,
  name: string.isRequired,
  onChange: func,
  minValue: number,
  maxValue: number,
  label: string,
  disabled: bool
};

NumberSelector.defaultProps = {
  defaultValue: 0,
  name: undefined,
  onChange: undefined,
  minValue: undefined,
  maxValue: undefined,
  label: '',
  disabled: false
};

export default NumberSelector;
