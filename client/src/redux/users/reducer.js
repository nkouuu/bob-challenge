import { CREATE_USER, GET_ALL_USERS } from './actions';

const usersReducer = (state = { data: [], error: null }, action) => {
  switch (action.type) {
    case GET_ALL_USERS:
      return { ...state, error: null, data: action.payload };
    case CREATE_USER:
      return { data: [action.payload, ...state.data], error: null };
    default:
      return state;
  }
};

export default usersReducer;
